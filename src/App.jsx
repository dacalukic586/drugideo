import React, {useState} from 'react';
import logo from './logo.svg';
import './App.css';


// /* <table>
//           <tr>
//           <th id="t">Naslov</th>
//           </tr>

//           <tr>
//             <th>Item1</th>
//           </tr>
//           <tr>
//             <th>Item2</th>
//           </tr>
//           <tr>
//             <th>Item3</th>
//           </tr>
//           </table>
//           */
// /*let width = someArray.length;
// let text = "<table>";
// for(let i = 0;i<width;i++)
// {
//   text+="<tr>" + "<th>" + someArray[i] +"<th>"+ "<tr>";
// }
// text+="</table>"


// <h1 style={{color:"red"}}>Naslov</h1>
//         {someArray.map((item) => <Odziv ime={item}/>)}
// function Odziv(props){
//   return <tr><th>{props.ime}</th></tr>;
// }
// */
// function AddMessageForm(props) {
//   const [ message, setMessage ] = useState('');
    
//   function promena(e) {
//     setMessage(e.target.value);
//   }
//   console.log(props)
    
//   function upisPromene(e) {
//     if(message !== '') {
//       props.upisPromene(message);
//       setMessage('');
//     }
//     e.preventDefault();
//   }
//   return (
//     <form onSubmit={upisPromene}>
//       <input type="text" 
//         placeholder="Add new message" 
//         onChange={promena} 
//         value={message} />
//       <button type="submit">Add</button>
//     </form>
//   );
// }
// function listaPoruka(props) {
//   const arr = props.data;
//   const listItems = arr.map((val, index) =>
//     <li key={index}>{val}</li>
//   );
//   return <ul>{listItems}</ul>;
// }
/*
function App() {
  const [messages, setMessages] = useState([]); 
  const [text, setText] = useState("")

  function handleAddMessage(e){
    e.preventDefault()
    setMessages(prevState => [...prevState, text]);
    setText("")
    
      
  }

  function promena(e) {
    const currentText = e.target.value
    setText(currentText);
  }
  console.log(messages)
  return (
    <div className='fisrt_form'>
      <div className='form_in_form'>
      <h1>ToDo</h1>
      <input type="text" placeholder="Add new message"  onChange={promena} value={text} />
        <button onClick={handleAddMessage}>Add</button>

        {messages.map((message, index) => {
          return <p key={index}>{message}</p>
        })}

        </div> 
  </div>
  );
}*/

function Todo({ todo, index, markTodo, removeTodo, nextTodo }) {
  const [buttonText,setButtonText] = useState("");
  const changeText = (text) => setButtonText(text);
  
  return (
    <><div className="todo">
      {todo.text}
      
        <button className='b1' onClick={() => markTodo(index)}>{todo.isDone? "todo":"next"}</button>
        <button className='b2' onClick={() => removeTodo(index)}>Remove</button>
    </div>
    </>
    
  );
}
function InProgress({todo,index,markTodo,nextTodo,backTodo,markTodoIP})
{
  const [buttonText,setButtonText] = useState("");
  const changeText = (text) => setButtonText(text);

  return (
    <><div className="todo1">
      {todo.text}
      <button className='b1' onClick={() => backTodo(index)}>{!todo.isDone? "todo":"next"}</button>
        <button className='b1' onClick={() => markTodoIP(index)}>{todo.nextDone? "next":"back"}</button>
        
    </div>
    </>
    
  );

}
function Comptetee({todo,index,markTodo,removeTodo,backTodo,backTodolast})
{
  const [buttonText,setButtonText] = useState("");
  const changeText = (text) => setButtonText(text);

  return (
    <><div className="todo1">
      {todo.text}
      
        
      <button className='b1' onClick={() => backTodo(index)}>{!todo.isDone? "":"back"}</button>
        <button className='b2' onClick={() => removeTodo(index)}>Remove</button>
    </div>
    </>
    
  );

}


function FormTodo({ addTodo }) {
  const [value, setValue] = React.useState("");

  const handleSubmit = e => {
    e.preventDefault();
    if (!value) return;
    addTodo(value);
    setValue("");
  };

  return (/*unosi se text */
    <div className='form'>
    <><form onSubmit={handleSubmit}>
      <input
        type="text"
        className="input"
        value={value}
        onChange={e => setValue(e.target.value)}
         />
    
    <button className='btn-3' onClick={handleSubmit}>Submit</button></form></>
    </div>
  );
}

function App() {
  const [todos, setTodos] = React.useState([
    {
      text: "This is a sampe todo",
      isDone: false,
      nextDone:false
    }
  ]);

  const addTodo = text => {/*dodaje se novi todo */
    const newTodos = [...todos, { text }];
    setTodos(newTodos);
  };

  const markTodo = index => {
    const newTodos = [...todos];
    newTodos[index].isDone = !newTodos[index].isDone;
    setTodos(newTodos);
  };

  const removeTodo = index => {
    const newTodos = [...todos];
    newTodos.splice(index, 1);
    setTodos(newTodos);
  };
  const nextTodo = index => {
    const newTodos = [...todos];
    newTodos[index].nextDone = !newTodos[index].nextDone;
    setTodos(newTodos);
   
  }
  const backTodo = index =>{
    const newTodos = [...todos];
    newTodos[index].nextDone = !newTodos[index].nextDone;
    setTodos(newTodos);
  }
  const markTodoIP = index => {
    const newTodos = [...todos];
    newTodos[index].isDone = !newTodos[index].isDone;
    setTodos(newTodos);
  };
  
  return (
    <div className="app">
      <div className="container">
        <h1 className="text-center mb-4">Todo List</h1>
        <FormTodo addTodo={addTodo} />
        <div className='todof'>
        <table>
            <thead>ToDo</thead>
            <tr>
              <td>
              {todos.map((todo, index) => (
              <>
                  {(!todo.isDone && <Todo
                    key={index}
                    index={index}
                    todo={todo}
                    markTodo={markTodo}
                    removeTodo={removeTodo}
                    />) 
                  }
                  </>
             ))}
              </td> 
            </tr>
                
          </table>
        </div>

         <div className="in-progress">
            <table>
              <thead>In progress</thead>
                <tr>
                  <td>
                  {todos.map((todo, index) => (
                  <>
                 {(!todo.nextDone && todo.isDone && <InProgress
                    key={index}
                    index={index}
                    todo={todo}
                    nextTodo = {nextTodo}
                    backTodo = {backTodo}
                    markTodoIP = {markTodoIP}
                  />) 
              
                  }
                  </>
                ))}
                  </td>
                </tr>
            </table>
        </div>
        
        <div className='complete'>
        <table>
            <thead>Complete</thead>
            <tr>
              <td>
              {todos.map((todo, index) => (
              <>
                  {(todo.nextDone && < Comptetee
                    key={index}
                    index={index}
                    todo={todo}
                    markTodo = {markTodo}
                    removeTodo={removeTodo}
                    backTodo = {backTodo}
                    />) 
                  }
                  </>
             ))}
              </td> 
            </tr>
                
          </table>
        </div>
      </div>
    </div>
  );
}

  
  
export default App;